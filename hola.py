import asyncio

async def hello():
    print('¡Hola ...')
    await asyncio.sleep(1)
    print('... mundo!')

async def main():
    await asyncio.gather(hello(), hello(), hello())  # Es una especie de run en paralelo.
    # Cuando están listas para ejecutar, vuelve a buscar el procesador y escribe "mundo", van haciendolo de una en
    # una (no hay un orden específico, el sistema elige una de ellas).

asyncio.run(main())
# Ejecutar corutina (main) y me espero al evento que ha terminado.
